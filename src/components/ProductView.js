import {useState, useEffect} from "react";
import React from "react";
import { useCart } from "react-use-cart";
import {Button, Nav } from "react-bootstrap"
import { Link } from "react-router-dom"


import Swal from "sweetalert2"

const Cart = (product) =>{

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [quantity, setQuantity] = useState(1);

	const [myProduct, setMyProduct] = useState([]);

	function addToCart(myProduct){
			Swal.fire({
					title: "Success",
					icon: "success",
					text: "Add to cart successfully!"
				})
			addItem(myProduct)
		}

	useEffect(() => {
	    setMyProduct({
	      	id:product.product.id,
	      	name: product.title,
	      	description:product.description,
	      	price:product.price
	    });
	}, []);

	const { addItem } = useCart()

	const { isEmpty, totalUniqueItems, items, totalItems, cartTotal, updateItemQuantity, removeItem, emptyCart } = useCart();

	return (
			<>
			<Button className="btn btn-success" onClick={() => addToCart(myProduct)}> Add to Cart</Button>
			<Button className="btn btn-danger"><Nav.Link as={Link} to="/products" eventKey="/products">Back</Nav.Link></Button>
			</>
		)
}
export default Cart;
