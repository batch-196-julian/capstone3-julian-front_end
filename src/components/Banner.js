import {Row, Col, Button, Card} from "react-bootstrap";
import {Link} from "react-router-dom";

export default function Banner({data}){


	const {title, content, destination, label} = data;

	return(
		
				<Row  className="pb-5 pt-2">
				<Card className="cardBanner">
					<Col lg="9" xl="12" className="pb-3 pt-3 text-center">
						<h1>{title}</h1>
						<p>{content}</p>
						<Button as={Link} to={destination} variant="primary">{label}</Button>
					</Col>
					</Card>
				</Row>

		
	)
}
