import React, {useEffect, useState, useContext} from "react";
import ProductCard from "../components/ProductCard";
import {Container, Row} from "react-bootstrap";
import { CartProvider } from "react-use-cart";

import UserContext from "../UserContext";


export default function Products(){

	const [products, setProducts] = useState([]);

	const { user } = useContext(UserContext)

	useEffect(() =>{
		fetch(`https://quiet-earth-27269.herokuapp.com/products/getAllActiveProduct`)
		.then(res => res.json())
		.then(data => {
			setProducts(data.map((product, index) =>{
				return(
					<ProductCard key={index} title={product.name} description={product.description} price={product.price} product={product} />
				)
			}))
		})
	},[])


	return(
		// (user.isAdmin)
		// ?
		// 	<Navigate to="/admin" />
		// :
		<>
		    <Container>
		        <CartProvider>
					<h1 className="p-5 text-center justify-content-md-center header-title">Products</h1>
					<Row className="mt-3 mb-3">
						{products}
					</Row>
		        </CartProvider>
		    </Container>
		</>
	)
}
