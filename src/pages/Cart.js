import React, {useContext} from "react";

import {Container} from "react-bootstrap";

import {Navigate} from "react-router-dom";

import { CartProvider } from "react-use-cart";

import UserContext from "../UserContext";

import CartComponent from "../components/Cart";

export default function Cart(){

	const { user } = useContext(UserContext)

	// const cartItem = localStorage.getItem('react-use-cart')


	return(

		(user.isAdmin)
		?
			<Navigate to="/admin" />
		: 
		<>
		    <Container>
		        <CartProvider>
		          <CartComponent />
		        </CartProvider>
		    </Container>
		</>
	)
}
