import Banner from '../components/Banner';
import CarouselSec from "../components/Carousel";
import Highlights from '../components/Highlights';

import { Container } from 'react-bootstrap';
import { Fragment } from 'react';



export default function Home() {
	const data = {
        title: "Julian Apparel",
        content: "The best way to shop online! Best and powerful platform for online shopping with better and faster shipping.",
        destination: "/products",
        label: "Buy Now!"
    }

    return (

        <Fragment>  
	        <Banner data={data}/>
            <Highlights/>
             <Container>
                <CarouselSec/>
            </Container>

		</Fragment>



    )
}
