import { Table, Button, Container, Navbar, NavbarBrand, Nav, NavItem } from 'react-bootstrap';
import { useState, useEffect, useContext } from "react";
import UserContext from "../UserContext"
import { Navigate, Link } from "react-router-dom";



export default function Order(){

  	// useContect
	const { user } = useContext(UserContext)


	// useState
	const [allProducts, setAllProducts] = useState([]);

	// "fetchData()" wherein we can invoke if their is a certain change with the course
	
	const fetchData = () => {
		fetch(`https://quiet-earth-27269.herokuapp.com/orders/showAllOrders`,
		{
			headers: {
				'Authorization': `Bearer ${localStorage.getItem("token")}`
				}
		})
		.then(res => res.json())
		.then(data => {
			setAllProducts(data.map(userOrders => {
				return(
					<tr key={userOrders._id}>
						    <td>{userOrders._id}</td>

						    <td>{userOrders.receiver}</td>
						    <td>{userOrders.contactNumber}</td>
						    <td>{userOrders.shippingAddress}</td>

						    <td>{userOrders.totalAmount}</td>
						   
					</tr>
				)
			}))
		})
	}




	useEffect(() => {
		fetchData();
	}, [])

	return(

		(user.isAdmin)
		?
		<>
			<Container className="pt-3">
				<Navbar >
					<Container>
						<NavbarBrand><h1 className="header-admin">Order Dashboard</h1></NavbarBrand>
						<Nav>
							<NavItem>
								<Button variant="primary" size="sm" as={Link} to={`/admin`} className="mx-2">Products</Button>
								<Button variant="success" size="sm" as={Link} to={`/order`} className="mx-2">Orders</Button>
							</NavItem>
						</Nav>
						
					</Container>
				</Navbar>
				{/*Table Start*/}
				<Table striped bordered hover>
				    <thead>
				        <tr> 	
				          	<th>Customer Name</th>
				          	<th>Receiver Name</th>
				          	<th>Contact Number</th>
				          	<th>Shipping Address</th>
				          	<th>Total Amount</th>
				        </tr>
				    </thead>
				    <tbody>
				        { allProducts }
				    </tbody>
				</Table>
				{/*Table End*/}
			</Container>

		</>
		:
		<Navigate to="/products" />
	)
}
