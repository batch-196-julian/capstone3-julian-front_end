import React, {useEffect, useState, useContext} from "react";
import {Navigate} from "react-router-dom";
import { Link, useParams} from "react-router-dom"
import { Container, Card, Button, Row, Col} from "react-bootstrap"
import { CartProvider } from "react-use-cart";
import UserContext from "../UserContext";
import ProductViewComponent from "../components/ProductView";
export default function Cart(){

	const { user } = useContext(UserContext)

	// const cartItem = localStorage.getItem('react-use-cart')

	const { productId } = useParams()

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [stocks, setStocks] = useState(0);
	const [imageURL, setImageURL] = useState('');
	// const [quantity, setQuantity] = useState(1);

	const [products, setProducts] = useState();


	// const i = cartItem.map(items => items)
	// console.log(i)

useEffect(() =>{	
		fetch(`https://quiet-earth-27269.herokuapp.com/products/getSingleProduct/${productId}`)
		.then(res => res.json())
		.then(data => {
			setProducts({
				id: productId,
				name: name,
				description: description,
				price: price,
				stocks: stocks,
				// quantity: 1,
				itemTotal: price,
			})
			setName(data.name);
			setDescription(data.description);
			setStocks(data.stocks);
			setPrice(data.price);
			setImageURL(data.imageURL);
		})
	}, [name, description, price, productId, stocks])
	return(

		(user.isAdmin)
		?
			<Navigate to="/admin" />
		: 
		<>
		    <Container>
		        <CartProvider>
		          
			          <Container className="mt-5">
			          	<Row>
			          		<Col className="col-md-6">
			          				<Card.Img variant="top" src={`https://drive.google.com/uc?export=view&id=${imageURL}`}  />
			          		</Col>
			          		<Col className="col-md-6">
			          			<Card.Body className="productCatalog">
			                          <div className="text">
			                              <h1>{name}</h1>
			                              <div className="price mt-4">
			                                  <h4 className="text-danger">&#8369;{price}</h4>
			                                  {
			                                      stocks > 0
			                                      ? <h6 className="text-danger">Stocks: {stocks}</h6>
			                                      : <h6 className="text-danger">Out Stock</h6>
			                                  }
			                              </div>
			                              <div className="description mb-5">
			                                  <p>{description}</p>		                    
			                              </div>

			          					<div className="d-grid gap-2">
			          					{ 
			          						(user.id !== null)
			          						?
			          							(stocks !== 0)
			          							?
			          								<ProductViewComponent description={description} title={name} price={price} product={products}/>
			          							:
			          								<Button variant="danger" size="lg">Out of Stock</Button>
			          						:
			          						<Button as={Link} to="/login" variant="primary" size="lg">Please login</Button>
			          					}
			          					</div>
			                          </div>
			          		    </Card.Body>
			          		</Col>
			          	</Row>
			          </Container>
		        </CartProvider>
		    </Container>
		</>
	)
}

