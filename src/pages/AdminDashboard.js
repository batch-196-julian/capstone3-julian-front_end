import { Table, Button, Form, Modal, Container, Navbar, NavbarBrand, Nav, NavItem } from 'react-bootstrap';
import { useState, useEffect, useContext } from "react";
import { Navigate, Link } from "react-router-dom";
import Swal from "sweetalert2";

import ProductData from "../components/ProductData";

import UserContext from "../UserContext"






export default function AdminDashboard(){

	// Modal
	const [ show, setShow ] = useState(false);
  	const handleClose = () => setShow(false);
  	const addProduct = () => setShow(true);


  	// useContect
	const { user } = useContext(UserContext)

	// useState
	const [allProducts, setAllProducts] = useState([]);
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [imageURL, setImageURL] = useState("");
	const [price, setPrice] = useState(0);
	const [stocks, setStocks] = useState(0);

	// "fetchData()" wherein we can invoke if their is a certain change with the course
	const fetchData = () => {
		fetch(`https://quiet-earth-27269.herokuapp.com/products/getAllActiveProduct`,
		{
			headers: {
				'Authorization': `Bearer ${localStorage.getItem("token")}`
				}
		})
		.then(res => res.json())
		.then(data => {
			setAllProducts(data.map(product => {
				return(
					<tr key={product._id}>
						<ProductData product={product} />
					</tr>
				)
			}))
		})
	}



	// onClick product function for add product
	function product(e){

		//prevents the page redirection via form submit
		e.preventDefault();

		fetch(`https://quiet-earth-27269.herokuapp.com/products`, {
			method: "POST",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price,
				stocks: stocks,
				imageURL: imageURL
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data){
				Swal.fire({
					title: "Successfully added",
					icon: "success",
					text: "You have successfully added new product"
				});

				fetchData();

			}
			else{
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again!."
				});
			}	
		})	

	}
		  


	useEffect(() => {
		fetchData();
	}, [])

	return(

		(user.isAdmin)
		?
		<>
			<Container className="pt-3">

				<Navbar >
					<Container>
						<NavbarBrand><h1 className="header-admin">Admin Dashboard</h1></NavbarBrand>
						<Nav>
							<NavItem>
								<Button variant="primary" size="sm" className="ms-auto" onClick={addProduct}>Add Product</Button>
								<Button variant="success" size="sm" as={Link} to={`/order`} className="ms-2">Orders</Button>
								<Button variant="secondary" size="sm" as={Link} to={`/users`} className="mx-2">Users</Button>
							</NavItem>
						</Nav>
						
					</Container>
				</Navbar>

				{/*Table Start*/}
				<Table striped bordered hover>
				    <thead>
				        <tr>
				          	<th>Product IDs</th>
				          	<th>Product Name</th>
				          	<th>Description</th>
				          	<th>Price</th>
				          	<th>Stocks</th>
				          	<th>Status</th>
				          	<th>Action</th>
				        </tr>
				    </thead>
				    <tbody>
				        { allProducts }
				    </tbody>
				</Table>
				{/*Table End*/}

			</Container>



			{/*Add Product Modal*/}
			<Modal show={show} onHide={handleClose}>
		        <Form onSubmit ={(e) => product(e)}>
			        <Modal.Header closeButton>
			          	<Modal.Title>Add Product</Modal.Title>
			        </Modal.Header>
			        <Modal.Body>
			            <Form.Group className="mb-3" controlId="imageURL">
			              	<Form.Label>Google Image ID</Form.Label>
			              	<Form.Control type="text" placeholder="Input Google Image ID" onChange={e => setImageURL(e.target.value)} autoFocus />
			            </Form.Group>
			            <Form.Group className="mb-3" controlId="name">
			              	<Form.Label>Product Name</Form.Label>
			              	<Form.Control type="text" placeholder="Input product name" onChange={e => setName(e.target.value)} />
			            </Form.Group>
			            <Form.Group className="mb-3" controlId="description">
			              	<Form.Label>Description</Form.Label>
			              	<Form.Control as="textarea" rows={4} onChange={e => setDescription(e.target.value)}/>
			            </Form.Group>
			            <Form.Group className="mb-3" controlId="price">
			              	<Form.Label>Price</Form.Label>
			              	<Form.Control type="number" placeholder="Input product price" onChange={e => setPrice(e.target.value)}/>
			            </Form.Group>
			            <Form.Group className="mb-3" controlId="stocks">
			              	<Form.Label>Stocks</Form.Label>
			              	<Form.Control type="number" placeholder="Input product quantity"  onChange={e => setStocks(e.target.value)}/>
			            </Form.Group>
			        </Modal.Body>
			        <Modal.Footer>
			          	<Button variant="secondary" onClick={handleClose}>
			            	Close
			          	</Button>
			          	<Button variant="primary" type="submit" id="submitBtn" onClick={handleClose}>
			            	Save
			          	</Button>
			        </Modal.Footer>
		        </Form>
	      	</Modal>

		</>
		:
		<Navigate to="/products" />
	)
}
