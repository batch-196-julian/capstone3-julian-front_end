import {Container} from 'react-bootstrap';
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import {useState, useEffect} from "react";
import { UserProvider } from "./UserContext";
// import { CartProvider } from "react-use-cart";



import AppNavbar from "./components/AppNavbar";


import AdminDashboard from "./pages/AdminDashboard";
import Cart from "./pages/Cart";
import Error from './pages/Error';
import Home from "./pages/Home";
import Login from "./pages/Login";
import Logout from "./pages/Logout";
import Order from "./pages/Order";
import Products from "./pages/Products";
import Register from "./pages/Register";
import UserList from "./pages/UserList";
import UserOrder from "./pages/UserOrder";
import ViewProduct from "./pages/ViewProduct";




import './App.css';


function App() {


  const [user, setUser] = useState({

    id: null,
    isAdmin: null
  })


  const [product, setProduct] = useState({

      id: null,
      updatedProduct: []
    })


  const unsetUser = () =>{
    localStorage.clear();
  }


  useEffect(() =>{
    fetch('https://quiet-earth-27269.herokuapp.com/users/userDetails',{
      headers:{
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    })
    .then(res => res.json())
    .then(data => {


      if(typeof data._id !== "undefined"){
          setUser({

            id: data._id,
            isAdmin: data.isAdmin
          })
      }
      else{
          setUser({
              id: null,
              isAdmin: null
            })
      }
      
    })
  }, [])

  return (

    
    <UserProvider value={{user, setUser, unsetUser, product}}>
        <Router>
          <AppNavbar />
          <Container>          
              <Routes> 
               <Route exact path = "/" element={<Home />} />
                  <Route exact path = "/admin" element={<AdminDashboard />} />
                  <Route exact path = "/order" element={<Order/>} />
                  <Route exact path = "/cart" element={<Cart />} />
                  <Route exact path = "/order/:id" element={<UserOrder />}/>
                  <Route exact path = "/products" element={<Products />} />
                  <Route exact path = "/products/:productId" element={<ViewProduct />} />
                  <Route exact path = "/register" element={<Register />} />
                  <Route exact path = "/login" element={<Login />} />
                  <Route exact path = "/logout" element={<Logout />} />
                  <Route exact path = "/users" element={<UserList />} />
                  <Route exact path = "*" element={<Error />} />
              </Routes>
          </Container>
        </Router>
    </UserProvider>
  );
}

export default App;
